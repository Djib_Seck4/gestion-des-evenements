<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public $timestamps = false;
    protected $fillable = ['nom', 'desc', 'lieu', 'prix','start_at'];

    // protected $dates = ['start_at']; //pr convertir start_at à une instance de type carbone( c'est comme DateTime mais c plus avancé)
    protected $casts = ['start_at' => 'datetime'];

    public function isFree()
    {
        return $this->prix == 0;
    }

    public function getPrixPlusAttribute()
    {
        return $this->attributes['prix'] + 100;
    }

    public function getPrixAttribute($value)
    {
        return $value;
    }
}
