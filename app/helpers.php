<?php
 //on p sup ce fic car on a creer un dos Helpers qui contient nos
 //helpers... mais on p le laisser en guise d'aide
if(! function_exists('formater_prix'))
{
    function formater_prix($evennement)
    {
        if($evennement->isFree())
        {
            return '<strong>GRATUIT!</strong>';
        }
        else
        {
            return sprintf('%.2f euros', $evennement->prix);
        }
    }
}

if(! function_exists('formater_date'))
{
    function formater_date($date)
    {
        return $date->format('d/m/Y H:i:s');
    }
}
?>