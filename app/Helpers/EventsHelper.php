<?php

namespace  app\Helpers;

class EventsHelper
{
    public static function formater_prix($evennement)
    { 
        if($evennement->isFree())
        {
            return '<strong>GRATUIT!</strong>';
        }
        else
        {
            return sprintf('%.2f euros', $evennement->prix);
        }
    }

    public static function formater_date($date)
    {
        return $date->format('d/m/Y H:i:s');
    }

}

?>