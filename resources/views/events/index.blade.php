<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Events</title>

</head>
<body>
    <h1> <center>{{ $events->count() }} Evennements</center></h1>

    @foreach($events as $evennement)
        <article>
            <h1> {{ $evennement->nom }}</h1>
            <p><u>Description</u> : {{ $evennement->desc }}</p>
            <p><u>Prix</u>: {!! $formater_prix($evennement) !!} </p>
            <p><u>Prix</u>: <strike>{{ $evennement->prix_plus }} </strike></p>
            <p><u>Lieu</u> :  {{ $evennement->lieu }}</p>
            <!-- <p><u>Date</u> :{{ $evennement->start_at->format('d/m/Y H:i:s') }}</p> -->
            <p><u>Date</u> :{{ $formater_date($evennement->start_at) }}</p>

            @if(! $loop->last)
                <hr>
            @endif
        </article>
        {{-- je suis un commentaire test --}}
    @endforeach
</body>
</html>
